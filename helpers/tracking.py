from typing import Any
import yaml
from pathlib import Path
from box import Box

from collections import namedtuple
from dataclasses import dataclass

import mido
from helpers.exceptions import BoardErrorSlotNotDefined


@dataclass
class Location:

    channel: int
    button: int
    type: str


@dataclass
class Slot:

    slot: int
    slot_type: str


class Board:

    slots: list
    channels: dict
    main_volume: int
    config: Box

    def __init__(self, config_file='settings.yaml') -> None:
        self.__parse_config(config_file)
        self.__build_channel_lut()
        self.__build_slot_lut()
        self.output_device = mido.open_output(self.config.device)
        print(self.config)

    def __parse_config(self, config_file: Path) -> None:
        config_file = Path(config_file)
        with config_file.open() as f:
            config = yaml.safe_load(f)
        self.config = Box(config)

    def __build_channel_lut(self) -> None:
        self.slots = dict()
        for channel, raw_slots in self.config.channels.items():
            slots = sorted([int(i) for i in raw_slots.split(',')], reverse=True)
            for button in range(0, len(slots)):
                self.slots[slots[button]] = {'channel': channel, 'button': button, 'type': 'sound'}

    def __build_slot_lut(self) -> None:
        self.channels = list()
        for channel, raw_slots in self.config.channels.items():
            array = sorted([int(i) for i in raw_slots.split(',')], reverse=True)
            array = [Slot(k, "sound") for k in array]
            self.channels.append(array)

    def lookup_button(self, slot: int) -> Location:
        try:
            return Location(**self.slots[slot])
        except KeyError:
            raise BoardErrorSlotNotDefined(slot=slot)

    def set_light(self, slot: int, velocity: int):
        msg = mido.Message('note_on', note=slot, velocity=velocity)
        self.output_device.send(msg)

    def lookup_slot(self, channel: int, button: int) -> int:
        return self.channels[channel][button]
