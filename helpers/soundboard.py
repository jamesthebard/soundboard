import sys
from pathlib import Path
from typing import Union, List

import mido
import yaml
from box import Box, BoxList
from rich import print


class Soundboard:
    config_file: Path
    config: Box
    channel_map: Box
    master: int
    scene_controls: list
    device: str
    exit: int
    slots: dict

    def __init__(self, config_file: Union[str, Path]):
        self.config_file = Path(config_file)
        self.channel_map = Box()
        self.slots = dict()
        self.parse_config_file()

    def parse_message(self, message):
        msg_type = getattr(message, 'type', None)
        msg_note = getattr(message, 'note', None)
        if msg_type == 'note_on' and msg_note == self.exit:
            print("Exiting!")
            sys.exit(0)

        channel = self.slots[msg_note]
        print("Channel: {}".format(channel))

    def parse_config_file(self):
        with self.config_file.open() as f:
            self.config = Box(yaml.safe_load(f))

        # Configure the slots for each channel
        for k, v in self.config.channels.items():
            self.channel_map[k] = dict()
            self.channel_map[k].slots = sorted([int(j) for j in v.split(',')], reverse=True)
            for slot in self.channel_map[k].slots:
                self.slots[slot] = k
    
        # Configure the volume slider for the channel
        levels = [int(i) for i in self.config.levels.split(',')]
        for i in range(0, len(levels)):
            self.channel_map[i].level = levels[i]

        # Configure the play/pause button for the channel
        controls = [int(i) for i in self.config.controls.split(',')]
        for i in range(0, len(controls)):
            self.channel_map[i].control = controls[i]
            self.slots[controls[i]] = i

        # Configure the master volume slider
        self.master = int(self.config.master)

        # Configure which buttons will control scenes
        self.scene_controls = [int(i) for i in self.config.scenes.split(',')]

        # The MIDI device that will be used for the soundboard
        self.device = self.config.device

        # Configure which button exits the soundboard
        self.exit = self.config.exit


if __name__ == '__main__':
    a = Soundboard(config_file='settings.yaml')
    print(a.channel_map)
    print(a.master)
    print(a.scene_controls)
    print(a.config)
    print(a.device)
    print(a.slots)