class BoardError(Exception):
    pass

class BoardErrorSlotNotDefined(BoardError):

    def __init__(self, slot: int) -> None:
        self.slot = slot
        self.message = "Slot {} is not defined in the configuration file.".format(self.slot)
        super().__init__()
