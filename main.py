import mido
from rich import print
from rich.traceback import install

from helpers.tracking import Board
from helpers.exceptions import *

install()

a = Board(config_file='settings.yaml')

try:
    print(a.lookup_button(50))
    print(a.lookup_button(100))
except BoardErrorSlotNotDefined as e:
    print(f"ERROR: {e.message}")
